(ns gmi2html.core-test
  (:require [clojure.test :refer :all]
            [clojure.string :as str]
            [gmi2html.core :as gmi]))

(defn wrap [input]
  (format "<div>%s</div>" input))

(deftest renders-h1
  (is (= (gmi/gmi->html "# Hello")
         (wrap "<h1>Hello</h1>"))))

(deftest renders-h2
  (is (= (gmi/gmi->html "## Hello")
         (wrap "<h2>Hello</h2>"))))

(deftest renders-h3
  (is (= (gmi/gmi->html "### Hello")
         (wrap "<h3>Hello</h3>"))))

(deftest renders-a
  (is (= (gmi/gmi->html (str/trim "
=> path.gmi Test link
=> page.gmi
"))
         (wrap "<div><a href=\"path.gmi\">Test link</a></div><div><a href=\"page.gmi\">page.gmi</a></div>"))))

(deftest renders-ul
  (is (= (gmi/gmi->html "* Hello
* There
* Friend")
         (wrap "<ul><li>Hello</li><li>There</li><li>Friend</li></ul>"))))

(deftest renders-pre
  (is (= (gmi/gmi->html "```
# ASCII ART
## NEAT
> Cool
```")
         (wrap "<pre>\n# ASCII ART\n## NEAT\n> Cool</pre>"))))

(deftest renders-blockquote
  (is (= (gmi/gmi->html "> Hello
> Hello Again
> Nice")
         (wrap (str "<blockquote style=\"white-space: pre-line\">"
                    "Hello\nHello Again\nNice"
                    "</blockquote>")))))

(deftest renders-page
  (is (= (gmi/gmi->html "# Cool page
## I made it myself

> Quote of the day
> Me

Hello

* List
* List 2

Hello again

=> blog.gmi My gemlog
=> reading.gmi Reading list

Nice")
         (wrap "<h1>Cool page</h1><h2>I made it myself</h2><p></p><blockquote style=\"white-space: pre-line\">Quote of the day\nMe</blockquote><p></p><p>Hello</p><p></p><ul><li>List</li><li>List 2</li></ul><p>Hello again</p><p></p><div><a href=\"blog.gmi\">My gemlog</a></div><div><a href=\"reading.gmi\">Reading list</a></div><p></p><p>Nice</p>"))))
