(ns gmi2html.core
  (:require [clojure.string :as str]
            [hiccup.core :as hiccup]))

(defn- match-prefix [prefix line]
  (-> line
       str/trim
       (str/starts-with? prefix)))

(defn- match-h3 [line]
  (match-prefix "###" line))

(defn- match-h2 [line]
  (match-prefix "##" line))

(defn- match-h1 [line]
  (match-prefix "#" line))

(defn- match-blockquote [line]
  (match-prefix ">" line))

(defn- match-li [line]
  (match-prefix "*" line))

(defn- match-a [line]
  (match-prefix "=>" line))

(defn- match-pre [line]
  (match-prefix "```" line))

(defn- clean-up-prefix [input prefix]
  (-> input
      (str/replace-first prefix "")
      str/trim))

(defn- a-get-href [input]
  (-> input
      (clean-up-prefix "=>")
      (str/split #" ")
      first))

(defn- a-get-text [input]
  (let [words (-> input
                  (clean-up-prefix "=>")
                  (str/split #" "))
        pruned (drop 1 words)
        joined (if (empty? pruned)
                 (first words) ; For when no text is provided
                 (str/join " " pruned))]
    joined))

(defn- append-to-last-text [vec2d value]
  (let [last-elem (last vec2d)
        tag (first last-elem)
        attr (second last-elem)
        text (nth last-elem 2)
        new-text (str text "\n" value)]
    (-> vec2d
        drop-last
        vec
        (conj [tag attr new-text]))))

(defn- append-to-last [vec2d value]
  (let [last-elem (last vec2d)
        vec-dropped (vec (drop-last vec2d))
        new-value (conj last-elem value)
        result (conj vec-dropped new-value)]
    result))

(defn- is-elem? [elem type]
  (if (and (vector? elem)
           (not-empty elem))
    (= (first elem) type)
    false))

(defn- match-line [results line]
  (let [last-match (last results)]
    (cond
      ;; Elements that accumulate under one tag (i.e. pre, blockquote, ul).
      ;; A nil is appended to the result vector to end accumulation. These nils are filtered
      ;; from the final results.
      (is-elem? last-match :pre) (if (match-pre line)
                                   (conj results nil)
                                   (append-to-last-text results line))
      (is-elem? last-match :blockquote) (if (match-blockquote line)
                                          (append-to-last-text results (clean-up-prefix line ">"))
                                          ;; Process this line with preceding nil to allow to allow
                                          ;; the following matchers to be evaluated.
                                          (match-line (conj results nil) line))
      (is-elem? last-match :ul) (if (match-li line)
                                  (append-to-last results [:li (clean-up-prefix line "*")])
                                  (conj results nil))
      
      ;; Simpile matchers
      (match-h3 line) (conj results [:h3 (clean-up-prefix line "###")])
      (match-h2 line) (conj results [:h2 (clean-up-prefix line "##")])
      (match-h1 line) (conj results [:h1 (clean-up-prefix line "#")])
      ;; Style blockquotes with "pre-line" whitespace so newlines are respected.
      (match-blockquote line) (conj results [:blockquote {:style "white-space: pre-line"}
                                             (clean-up-prefix line ">")])
      (match-a line) (conj results [:div [:a {:href (a-get-href line)} (a-get-text line)]])
      (match-pre line) (conj results [:pre {} ""])
      (match-li line) (conj results [:ul {} [:li (clean-up-prefix line "*")]])
      :else (conj results [:p line]))))

(defn gmi->hiccup [gmi]
  (let [lines (str/split-lines gmi)]
    (into [:div] (->> lines
                      (reduce match-line [])
                      (filter identity)))))

(defn gmi->html [input]
  (hiccup/html (gmi->hiccup input)))
